# Social Login


## Login com Facebook
* Crie um app em: https://developers.facebook.com/apps/
  * No app criado **+ add Product** e adicione o produdo **Facebook Login**
    * Nas configurações do **Facebook Login** em **Valid OAuth redirect URIs** adicione a url da aplicação ex.: https://evadigital.com.br/
    
  * Nas configurações do APP **Settings** em  **App Domains** adicione o domínio da aplicação ex.: evadigital.com.br


* Configurar o provider do Facebook em ./server/providers.json **facebook-login**
  * adicione o **clientID** e **clientSecret**, "essas informações podem ser encontradas na tela de configuração do app no Facebook como **App ID** e **App Secret**"
  * **scope** é um array com as lista de permissões a serem requisitadas no login. Lista com todas as permissões: (https://developers.facebook.com/docs/facebook-login/permissions)
  * URLs **callbackURL, authPath, callbackPath, successRedirect e failureRedirect**, para configurar as rotas de autenticação, callback e redirecionamento caso necessário.
  
Para executar o login com Facebook basta chamar a URL do **authPath** "/auth/facebook", será redirecionado para página de login do Facebook e se já estiver logado pedirá permissão de acesso as informações do **scope**.



## Login com Google+
* Crie um projeto em: https://console.developers.google.com/
  * Em **Ativar APIS e Serviços** procure por todas e ative **Google+ API**
    * Em **Credenciais** crie um credencial **ID do cliente OAuth**, **Aplicativo Web** e preencha as informações **Origens JavaScript autorizadas** (URL do domínio) e **URIs de redirecionamento autorizados** (URL de callback)


* Configurar o provider do Google+ em ./server/providers.json **google-login**
  * adicione o **clientID** e **clientSecret**, "essas informações aparecem apos criar a credencial do Google+ API como **ID do cliente** e **chave secreta do cliente**  
  * **scope** é um array com as lista de permissões a serem requisitadas no login.
  * URLs **callbackURL, authPath, callbackPath, successRedirect e failureRedirect**, para configurar as rotas de autenticação, callback e redirecionamento caso necessário.

Para executar o login com Google+ basta chamar a URL do **authPath** "/auth/google", será redirecionado para página de login do Google+ e se já estiver logado pedirá permissão de acesso as informações do **scope**.




## Login com Twitter
* Crie um app em: https://apps.twitter.com/
  * Preencher as informações do website e url de callback
  
* Configurar o provider do Twitter+ em ./server/providers.json **twitter-login**
  * adicione o **consumerKey** e **consumerSecret**, essas informações estão na guia **Keys and Access Tokens**  
  * URLs **callbackURL, authPath, callbackPath, successRedirect e failureRedirect**, para configurar as rotas de autenticação, callback e redirecionamento caso necessário.

Para executar o login com Twitter basta chamar a URL do **authPath** "/auth/twitter", será redirecionado para página de login do Twitter e se já estiver logado pedirá permissão de acesso.


## Login com Linkedin
* Crie um app em: https://www.linkedin.com/developer/apps peenchendo todas as informações
  * Configure as permições em **Permissões padrão do aplicativo** e a URL de callback **OAuth 2.0** 


* Configurar o provider do Linkedin em ./server/providers.json **linkedin-login**
  * adicione o **clientID** e **clientSecret**, essas informações estão em Autenticação, **Código de cliente e Segredo de cliente**
  * URLs **callbackURL, authPath, callbackPath, successRedirect e failureRedirect**, para configurar as rotas de autenticação, callback e redirecionamento caso necessário.

Para executar o login com Linkedin basta chamar a URL do **authPath** "/auth/linkedin", será redirecionado para página de login do Linkedin e se já estiver logado pedirá permissão de acesso as informações do **scope**.



### Nota:
**As models ./server/models são as originais do componente "loopback-component-passport" foram movidas para lá pois não estavam sendo reconhecidas diretamente do diretório "./node_modules/loopback-component-passport/lib/models"**

_Remover fake_db.json em produção, foi criado para facilitar a visualização da criação dos usuários (user, accessToken, userIdentity) pelo provider do passport_

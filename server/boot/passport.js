const loopbackPassport = require('loopback-component-passport'),
  {ensureLoggedIn} = require('connect-ensure-login');


module.exports = function (server) {
  // Passport configurators..
  const router = server.loopback.Router(),
    {PassportConfigurator} = loopbackPassport,
    passportConfigurator = new PassportConfigurator(server);


  server.middleware('auth', server.loopback.token({
    model: server.models.accessToken,
  }));


  passportConfigurator.init();
  passportConfigurator.setupModels({
    userModel: server.models.user,
    userIdentityModel: server.models.userIdentity,
    userCredentialModel: server.models.userCredential,
  });

  let config = {};
  try {
    config = require('../providers.json');
  } catch (err) {
    throw err;
  }

  for (const s in config) {
    const c = config[s];
    c.session = c.session !== false;
    passportConfigurator.configureProvider(s, c);
  }

  router
    .get('/auth/account', ensureLoggedIn('/'), function (req, res) {
      res.json({
        user: req.user,
        url: req.url
      });
    })
    .get('/auth/logout', function (req, res) {
      req.logout();
      res.redirect('/');
    });


  server.use(router);
};

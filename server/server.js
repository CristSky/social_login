'use strict';

const loopback = require('loopback'),
  boot = require('loopback-boot'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  flash = require('express-flash'),


  app = module.exports = loopback();

app.use(flash());
app.middleware('session:before', cookieParser('ad51883a-5941-65f4-a468-197ad54eaa56'));
app.middleware('session', session({
  secret: 'kitty',
  saveUninitialized: true,
  resave: true,
}));


app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
